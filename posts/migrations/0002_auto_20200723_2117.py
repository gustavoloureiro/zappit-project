# Generated by Django 3.0.4 on 2020-07-24 00:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='post',
            old_name='poser',
            new_name='poster',
        ),
    ]
